# Asn1cpp

This is done foz KZPS (kontrola zračnega prometa Slovenije), where asn1c is being used for message encoding. Decision was made, to 
create nicer cpp bindings and this library is used, to create those.

## Requirements: 

* python3
* g++
* python libraries: pycparser and pycparserext
* testing: libgtest.a in test directory
* testing: premake5

## Usage

    python3 main.py --help

Example:

    python3 main.py -CXX -fcompound-names My.asn

main.py can be used to just run asn1c, if -CXX is not used.

## Test

    cd test;
    ./build_tests.sh
    make
    ./build/Asn1cppTests

