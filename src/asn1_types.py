import pycparser
import pycparserext

ANCESTORS = {"asn1::Enum", "asn1::SequenceOf", "asn1::BitString", "asn1::Enum", "asn1::Wrapper",
             "asn1::BitString", "asn1::Choice", "asn1::Integer", "asn1::Long",
             "asn1::IA5String", "asn1::OctetString", "asn1::Null"}

INHERITS = {
    "INTEGER_t": "asn1::Integer",
    "long": "asn1::Long",
    "OCTET_STRING_t": "asn1::OctetString",
    "IA5String_t": "asn1::IA5String",
    "NULL_t": "asn1::Null",
    "GeneralizedTime_t": "asn1::GeneralizedTime",
    "BOOLEAN_t": "asn1::Boolean"
}
inherits = INHERITS.copy()

def _upper(x):
    return x[0].upper() + x[1:]


def _lower(x):
    return x[0].lower() + x[1:]


class AsnType:
    def __init__(self, node, *_):
        self.node = node
        self.type = self.node.name
        self.name = (self.type[:-2] if self.type.endswith("_t") else self.type)

    def parent(self):
        pass

    def _check(self, node, *other):
        pass

    def children(self):
        pass

    @classmethod
    def check(cls, node, *other):
        try:
            return cls._check(node, *other)
        except AttributeError:
            return False

    def gen_constructors(self, additional_template=None, additional_ctr=None, simple=False):
        class_name = self.name
        asn_def = "asn_DEF_" + self.name

        add_ctr = "" if additional_ctr is None else ", " + additional_ctr
        inheriting = self.parent() + ("<" + self.type +
                                      (", " + additional_template if additional_template is not None else "") +
                                      ">" if not simple else "")

        ns = "namespace asn1\n{"
        c0 = "class " + class_name + ": public " + inheriting + "\n  {\n  public:"
        c1 = class_name + "(" + self.type + "* t): " + inheriting + "(t, &" + asn_def + add_ctr + ") {}"
        c2 = class_name + "(): " + inheriting + "(0, &" + asn_def + add_ctr + ") {}"

        return ns + "\n  " + c0 + "\n    " + c1 + "\n    " + c2 + "\n"

    @staticmethod
    def gen_tail():
        return "  };\n}"

    @staticmethod
    # order is important!!
    def get_asn_type(node, enum):
        if AsnEnumerated.check(node, enum):
            return AsnEnumerated(node, enum)
        elif AsnSequenceOf.check(node):
            return AsnSequenceOf(node)
        elif AsnSimple.check(node):
            return AsnSimple(node)
        elif AsnBitString.check(node):
            return AsnBitString(node, enum)
        elif AsnChoice.check(node):
            return AsnChoice(node)
        else:
            return AsnSequence(node)

    @staticmethod
    def get_real_parents(asn_types):
        global inherits

        types = []
        for t in asn_types:
            if t.parent() in ANCESTORS:
                inherits[t.type] = t.parent()
            elif t.type not in ANCESTORS:
                types.append(t)

        max_iter = 10
        for it in range(max_iter):
            to_remove = []
            for t in types:
                if t.parent() in inherits.keys():
                    inherits[t.type] = inherits[t.parent()]
                    to_remove.append(t)

            for r in to_remove:
                types.remove(r)

            if len(types) == 0:
                break

            if it == max_iter - 1:
                raise RuntimeError("Max iter exceeded when searching real parents")

        for t in asn_types:
            if t.parent() != inherits[t.type]:
                t.set_parent(inherits[t.type])

    # TODO!!
    @staticmethod
    def subtype(module, getter):
        obj = AsnType.get_asn_type(module, None)
        typ = str(module.type.type).split(".")[2].split(" ")[0]

        if typ == "Struct":
            upper = module.name
            getter = getter + "." + module.name
            return obj.generate_getters((upper, getter))
        else:
            raise ValueError("Invalid type of subtype {}, from {}".format(typ, module.type.type))


class AsnSequenceOf(AsnType):
    @classmethod
    def _check(cls, node, *other):
        in_struct = node.type.type.decls
        if len(in_struct) == 2 and in_struct[0].name == 'list':
            return True
        return False

    def generate_cpp_bind(self):
        lst = self.node.type.type.decls[0]
        assert (lst.name == "list")

        lst_type = lst.type.type.decls[0].type.type
        # looks like pycparser is not really consistent between releases or something. This handles one error
        if "name" not in lst_type.attr_names and "names" not in lst_type.attr_names:
            lst_type = lst_type.type.type

        if "names" in lst_type.attr_names:
            name = inherits[lst_type.names[0]]
        else:
            name = "asn1::" + lst_type.name

        return self.gen_constructors(additional_template=name) + self.gen_tail()

    def parent(self):
        return "asn1::SequenceOf"


class AsnSimple(AsnType):
    def __init__(self, node):
        super().__init__(node)
        self._parent = self.node.type.type.names[0]

    @classmethod
    def _check(cls, node, *other):
        tt_names = node.type.type.names
        if len(tt_names) == 1 and tt_names[0] not in ["ENUMERATED_t", "BIT_STRING_t"]:
            return True
        return False

    def generate_cpp_bind(self):
        return self.gen_constructors(self.parent(), simple=True) + self.gen_tail()

    def parent(self):
        return self._parent

    def set_parent(self, p):
        self._parent = p


class AsnEnumerated(AsnType):
    def __init__(self, node, enum):
        super().__init__(node)
        self._enum = enum

    @classmethod
    def _check(cls, node, *other):
        enum = other[0]
        tt_names = node.type.type.names
        if len(tt_names) == 1:
            return tt_names[0] == "ENUMERATED_t" or \
                   tt_names[0] == "long" and enum is not None

        return False

    def generate_cpp_bind(self):
        first_enum = self._enum.type.type.values.enumerators[0].name
        add_template = "e_" + self.name + ", " + self.name
        s = self.gen_constructors(additional_template=add_template, additional_ctr=first_enum)

        return s + self.gen_tail()

    def parent(self):
        return "asn1::Enum"


class AsnBitString(AsnType):
    def __init__(self, node, bits):
        super().__init__(node)
        self._bits = bits

    @classmethod
    def _check(cls, node, *other):
        tt_names = node.type.type.names
        if len(tt_names) == 1 and tt_names[0] == "BIT_STRING_t":
            return True
        return False

    def generate_cpp_bind(self):
        bits = self._bits.type.type.values.enumerators
        num_bits = max([int(i.value.value) for i in bits]) + 1
        add_templates = "e_{}, {}".format(self.name, num_bits)

        return self.gen_constructors(additional_template=add_templates) + self.gen_tail()

    def parent(self):
        return "asn1::BitString"


class AsnChoice(AsnType):
    @classmethod
    def _check(cls, node, *other):
        decls = node.type.type.decls
        pr_name = node.type.type.name + "_PR"
        if len(decls) == 3 and decls[0].name == "present":
            if decls[0].type.type.names[0] == pr_name:
                return True
        return False

    def generate_cpp_bind(self):
        add_templates = "{}_PR".format(self.name)
        s = self.gen_constructors(additional_template=add_templates)

        for is_internal, othr in self.children():
            if is_internal:
                name, internals = othr
                s += ("    class Internal_{0}\n" +
                      "    {{\n" +
                      "    public:\n" +
                      "        Internal_{0}({1}* f) : father(f) {{}}\n" +
                      "        {1}* father;\n").format(name, self.name)

                for line in internals.split("\n"):
                    s += "    " + line + "\n"

                s += "    }};\n    Internal_{0} {1}() {{ return Internal_{0}(this); }}\n".format(name, _upper(name))
            else:
                name, typ = othr
                s += "    {} {}()\n".format(typ, _upper(name)) + \
                     "    {\n" + \
                     "        _check_type({}_PR_{});\n".format(self.name, _lower(name)) + \
                     "        {} ret(&internal()->choice.{});\n".format(typ, name) + \
                     "        ret.set_phony_parent();\n" + \
                     "        return ret;\n" + \
                     "     }\n"

        return s + self.gen_tail()

    def children(self):
        itr = self.node.type.type.decls[1].type.type.decls
        for c in itr:
            name = c.name
            is_internal = "name" in c.type.type.attr_names

            typ = c.type.type.name if is_internal else c.type.type.names[0]
            typ = "asn1::" + typ[:-2] if typ not in INHERITS.keys() else INHERITS[typ]

            if is_internal:
                subtype = self.subtype(c, "choice")
                yield True, (name, subtype)
            else:
                yield False, (name, typ)

    def parent(self):
        return "asn1::Choice"


class AsnSequence(AsnType):
    def parent(self):
        return "asn1::Wrapper"

    def generate_cpp_bind(self):
        s = ""
        s += self.gen_constructors(additional_ctr="_fill")
        s += self.generate_getters()
        s += self.generate_default()
        s += self.generate_filler()
        s += self.gen_tail()
        return s

    def generate_getters(self, upper_getter=None):
        s = ""
        for typ, name, isptr_bool in self.children():
            upper_name = name[0].upper() + name[1:]
            isptr = "&" if not isptr_bool else ""

            if upper_getter is not None:
                internal = "{}father->internal()->{}.{}".format(isptr, upper_getter[1], name)
            else:
                internal = "{}internal()->{}".format(isptr, name)

            parent = "ret.set_" + ("parent(&{})".format(internal) if isptr_bool else "phony_parent()")
            if upper_getter is not None:
                s += ("    {0} {3}()\n" +
                      "    {{\n" +
                      "        father->fill_if_empty();\n" +
                      "        {0} ret({5});\n"
                      "        {6};\n" +
                      "        return ret;\n" +
                      "    }}\n"
                      ).format(typ, upper_getter[1], name, upper_name, isptr, internal, parent)
            else:
                s += ("    {0} {2}()\n" +
                      "    {{\n" +
                      "        fill_if_empty();\n" +
                      "        {0} ret({4});\n"
                      "        {5};\n" +
                      "        return ret;\n" +
                      "    }}\n"
                      ).format(typ, name, upper_name, isptr, internal, parent)

        return s

    def generate_default(self):
        s = "    void set_default()\n" + \
            "    {\n" + \
            "        if(!is_empty())\n" + \
            "        {\n"

        w = "            "
        for _, name, _ in self.children():
            upper_name = name[0].upper() + name[1:]
            s += "{}{}().set_default();\n".format(w, upper_name)

        return s + \
            "        }\n" + \
            "    }\n"

    def generate_filler(self):
        return "  private:\n" + \
               "    static void _fill(Wrapper<" + self.type + ">* w)\n" + \
               "    {\n" + \
               "        w->fill(true);\n" + \
               "        (({}*)w)->set_default();\n".format(self.name) + \
               "    }\n"

    def children(self):
        for c in self.node.type.type.decls:
            # this can be done better!
            typ = c.type.type.name if "name" in c.type.type.attr_names else \
                c.type.type.names[0] if "names" in c.type.type.attr_names else \
                c.type.type.type.name if "name" in c.type.type.type.attr_names else \
                c.type.type.type.names[0]

            if typ == "asn_struct_ctx_t":
                continue

            typ = "asn1::" + typ if typ not in INHERITS.keys() else INHERITS[typ]
            typ = typ[:-2] if typ.endswith("_t") else typ
            is_optional = isinstance(c.type, pycparser.c_ast.PtrDecl) or isinstance(c.type, pycparserext.ext_c_parser.TypeDeclExt)

            nam = c.name
            yield typ, nam, is_optional
