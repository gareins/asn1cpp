#include <stdio.h>
#include <cstring>
#include <sstream>
#include <stdexcept>
#include <vector>
#include <cmath>
#include <ctime>
#include <cassert>
#include <sys/time.h> //TODO: make windows compatible!

namespace asn1
{

#ifndef ASN1CPP_HPP
#define ASN1CPP_HPP

#define no_asn_def "??NO_ASN_DEF??"
#define ERROR_STRING_BUFFER_SIZE 1000

/**
 * Defines encoding/decoding type
 */
typedef enum {
    BER, /// Basic Encoding Rules (for encoding, this uses DER subset)
    XER  /// XML Encoding Rules
} coding_t;

/**
 * \class Wrapper
 * \brief parent class for all asn1 generated classes.
 *
 * template T is the internal type, that is used as data container for a given type.
 */
template<typename T>
class Wrapper
{
public:
    /**
     * \brief constructor
     * \param t pointer (can be NULL) to actual Foo_t data type, where asn1c
     *          stores data for given class
     * \param desc pointer to type descriptor (usually asn_DEF_foo
     * \param fill_func custom fill functions
     *
     * fill_func used, because I am not able to override template class methods
     * and also am too lazy to learn something like type erasure
     */
    Wrapper(T* t, asn_TYPE_descriptor_t* desc, void (*fill_fun)(Wrapper<T>*)=0) :
        _is_empty(t == 0), _parent(NULL), _fill_fun(
            fill_fun)
    {
        if (desc != NULL) {
            _type_desc = *desc;
        } else {
            std::memset(&_type_desc, 0, sizeof(_type_desc));
            _type_desc.name = no_asn_def;
        }

        set_internal(t);
    }

    /**
     * \brief copy constructor
     */
    Wrapper(const Wrapper& other)
    {
        _type_desc = other._type_desc;
        _fill_fun = other._fill_fun;
        _parent = NULL;
        _is_empty = true;

        if(!other.is_empty()) {
            std::vector<uint8_t> ber = other.to_ber();
            from_ber(ber);
        } else {
            _internal = NULL;
        }
    }

    /**
     * \brief destructor
     *
     * if the object has a parent, internals are not freed, since the parent
     * will be freeing and double free would occur.
     */
    virtual ~Wrapper()
    {
        clear();
    }

    /**
     * \brief getter for the internal data
     */
    inline T* internal()
    {
        return _internal;
    }

    /**
     * \brief getter for the is_empty private boolean
     */
    inline bool is_empty() const
    {
        return _is_empty;
    }

    /**
     * \brief checks if object has a type descriptor. Used for error checking
     */
    inline bool has_desc() const
    {
        return strcmp(_type_desc.name, no_asn_def) != 0;
    }

    /**
     * \brief uses type descriptor to free internals
     */
    void clear()
    {
        if (has_parent() || !has_desc() || _is_empty) {
            return;
        }
        force_clear();
    }

    /**
     * \brief fills internals
     * \param run_default forces default fill() to be run
     *
     * if _fill_fun is set via constructor (see bit string), and run_default is
     * false, then default fill() is run, else _fill_fun is run and is_empty is
     * not set to false.
     */
    void fill(bool run_default = false)
    {
        if (_fill_fun && !run_default) {
            _fill_fun(this);
        } else {
            T* naked_internal = (T*) calloc(1, sizeof(T));
            set_internal(naked_internal);
            _is_empty = false;
            if(has_parent()) {
                assert(_parent != (T**)1); // parent should not be phony!
                *_parent = _internal;
            }
        }
    }

    /**
     * \brief returns type descriptor if there is any
     */
    asn_TYPE_descriptor_t* description() const
    {
        if (!has_desc()) {
            throw std::runtime_error("Class does not contain type description");
        }
        return (asn_TYPE_descriptor_t*)(&_type_desc);
    }

    /**
     * \brief converts object to vec<u8> BER representation.
     */
    std::vector<uint8_t> to_ber() const
    {
        throw_if_empty();
        std::vector<uint8_t> buffer;
        asn_enc_rval_t ret;

        // encoding changes description(), so a copy is given.
        asn_TYPE_descriptor_t type_desc_cpy = *description();
        ret = der_encode(&type_desc_cpy, internal_const(), ber2vec, &buffer);

        if (ret.encoded >= 0) {
            return buffer;
        } else {
            std::stringstream ss;
            ss << "BER ENCODE: \nFailed to encode: ";
            ss << ret.failed_type ?
               std::string(ret.failed_type->name) : "unknown";

            throw std::runtime_error(ss.str().c_str());
        }
    }

    /**
     * \brief converts object to string XER representation.
     */
    std::string to_xer() const
    {
        throw_if_empty();
        std::stringstream buffer;
        asn_enc_rval_t ret;

        // encoding changes description(), so a copy is given.
        asn_TYPE_descriptor_t type_desc_cpy = *description();
        ret = xer_encode(&type_desc_cpy, internal_const(), XER_F_BASIC, xer2sstream,
                         &buffer);

        if (ret.encoded >= 0) {
            return buffer.str();
        } else {
            std::stringstream ss;
            ss << "XER ENCODE: \nFailed to encode: ";
            ss << ret.failed_type ?
               std::string(ret.failed_type->name) : "unknown";

            throw std::runtime_error(ss.str().c_str());
        }
    }

    /**
     * \brief fills object with data from BER buffer
     */
    bool from_ber(const void* buffer, size_t buf_size, std::string* err=NULL)
    {
        // first clean old data
        clear();

        asn_dec_rval_t decode_result;
        bool constrains_ok;
        std::string constraints_err;
        T* naked_internal = 0;

        decode_result = description()->ber_decoder(0, description(), (void**) &naked_internal, buffer,
                        buf_size, 0);
        if (decode_result.code == RC_OK) {
            set_internal(naked_internal);
            _is_empty = false;
            constrains_ok = check_constraints(constraints_err);
            if (constrains_ok) {
                return true;
            }
        }

        // else
        force_clear();
        if(err) {
            std::stringstream ss;
            ss << "BER DECODE: \nFailed to decode: ";
            if (!constrains_ok) {
                ss << std::string("Constrain check:\n  ") << constraints_err;
            } else if (decode_result.code == RC_WMORE) {
                ss << std::string("Expected more bytes");
            } else {
                ss << std::string("unknown");
            }
            *err = ss.str();
        }
        return false;
    }

    /**
     * \brief fills object with data from XER string
     */
    bool from_xer(const void* buffer, size_t buf_size, std::string* err=NULL)
    {
        // first clean old data
        clear();

        asn_dec_rval_t decode_result;
        bool constrains_ok;
        std::string constraints_err;
        T* naked_internal = 0;

        decode_result = description()->xer_decoder(0, description(), (void**) &naked_internal, 0, buffer, buf_size);
        if (decode_result.code == RC_OK) {
            set_internal(naked_internal);
            _is_empty = false;
            constrains_ok = check_constraints(constraints_err);
            if (constrains_ok) {
                return true;
            }
        }

        // else
        force_clear();
        if(err) {
            std::stringstream ss;
            ss << "XER DECODE: \nFailed to decode: ";
            if (!constrains_ok) {
                ss << std::string("Constrain check:\n  ") << constraints_err;
            } else if (decode_result.code == RC_WMORE) {
                ss << std::string("Expected more bytes");
            } else {
                ss << std::string("unknown");
            }
            *err = ss.str();
        }
        return false;
    }

    // fills object with data from BER vector of bytes
    inline bool from_ber(std::vector<uint8_t>& data, std::string* err=NULL)
    {
        return from_ber(&data[0], data.size(), err);
    }
    // fills object with data from XER vector of bytes
    inline bool from_xer(std::string data, std::string* err=NULL)
    {
        return from_xer(&data[0], data.length(), err);
    }

    /**
     * \brief fills object with data from buffer.
     * \param buffer input data
     * \param buf_size size of data in bytes
     * \param mode decoding mode: XER/BER
     * \return true if decoding successful
     */
    bool decode(const void* buffer, size_t buf_size, coding_t ct, std::string* err=NULL)
    {
        if(ct == XER) {
            return from_xer(buffer, buf_size, err);
        } else if(ct == BER) {
            return from_ber(buffer, buf_size, err);
        } else {
            throw std::runtime_error("illegal mode!");
        }
    }

    /**
     * \brief check constraints of object. Throws runtime_error if empty
     */
    bool check_constraints() const
    {
        std::string s;
        return check_constraints(s);
    }

    /**
     * \brief check constraints of object. Throws runtime_error if empty
     * \param err string that gets filled with err msg
     */
    bool check_constraints(std::string& err) const
    {
        throw_if_empty();

        static char err_buff[ERROR_STRING_BUFFER_SIZE];
        size_t err_len = ERROR_STRING_BUFFER_SIZE;
        int ok = asn_check_constraints(description(), internal_const(), err_buff, &err_len);

        if(ok != 0) {
            err = std::string(err_buff, err_len);
        }
        return ok == 0;
    }

    /**
     * \brief sets phony parent, so that object is not cleaned when freeing
     */
    void set_phony_parent()
    {
        _parent = (T**)1;
    }

    /**
     * \brief sets parent, so that object is not cleaned when freeing
     *
     * also, if object is filled, that information can be forwarded to its
     * parent. Used for optional fields in SEQUENCEs
     */
    void set_parent(T** parent)
    {
        _parent = parent;
    }

    /**
     * \brief true if element has a parent
     */
    bool has_parent() const
    {
        return _parent != NULL;
    }

    /**
     * \brief overwrites internals with data
     */
    inline void set_internal(T* data)
    {
        _internal = data;
    }

    /**
     * \brief used for error strings, pure virtual
     */
    std::string type() const
    {
        return std::string(description()->name);
    }

    /**
     * \brief uses default value for enum and compound types
     *
     * used because of lack of simple copy functionality, default must be chosen
     * in order for from_ber(to_ber()) copy implementation to work
     */
     void set_default()
     {
     }

protected:
    /**
     * \brief used for error checking, throws runtime_error if empty
     */
    void throw_if_empty() const
    {
        if (is_empty()) {
            char ret[50];
            sprintf(ret, "%s does not exist!", type().c_str());
            throw std::runtime_error(ret);
        }
    }

    /**
     * \brief used for lazy allocation
     */
    void fill_if_empty()
    {
        if (is_empty()) {
            fill();
        }
    }

    /// internal asn1c data
    T* _internal;
    /// internal asn1c descriptor for constraints, decode/encode, freeing,... operations
    asn_TYPE_descriptor_t _type_desc;
    /// optional types can be empty, this bool handles that
    bool _is_empty;
    /// if has parent, object is freed by the parent and not by itself. This handles that
    T** _parent;

private:
    /**
     * \brief function used inside asn1c to convert XER output to string
     * \param buffer generated buffer to be written out
     * \param size usually size of the buffer
     * \param app_key void* pointer to stringstream, where the buffer is flushed to
     */
    static int xer2sstream(const void *buffer, size_t size, void *app_key)
    {
        std::stringstream *stream = (std::stringstream*) app_key;
        std::string s((const char*) buffer, size);
        *stream << s;
        return 0;
    }

    /**
     * \brief function used inside asn1c to convert BER output to vector of bytes
     * \param buffer generated buffer to be written out
     * \param size usually size of the buffer in bytes
     * \param app_key void* pointer to std::vector<uint8_t>, where the buffer is copied to.
     */
    static int ber2vec(const void* buffer, size_t size, void *app_key)
    {
        std::vector<uint8_t> *stream = (std::vector<uint8_t>*) app_key;
        const uint8_t* b = (const uint8_t*) buffer;
        for (uint i = 0; i < size; i++) {
            stream->push_back(b[i]);
        }
        return 0;
    }

    /**
     * \brief returns internal() as const method
     *
     * should be used with caution, since this function does not
     * actually guarantee constantness!
     */
    T* internal_const() const
    {
        return _internal;
    }

    /**
     * \brief uses type descriptor to free internals
     */
    inline void force_clear()
    {
        description()->free_struct(description(), internal(), 0);
        _is_empty = true;
    }


    /// special fill function
    void (*_fill_fun)(Wrapper<T>*);
};

/**
 * \class IntegerBase
 * \brief interface classed implemented by both integer internal types
 */
class IntegerBase
{
public:
    /// returns long value of the object
    virtual long get() = 0;
    /// sets long value of the object
    virtual void set(long l) = 0;

    /// returns unsigned long value of the object
    virtual unsigned long get_unsigned() = 0;
    /// sets unsigned long value of the object
    virtual void set_unsigned(unsigned long l) = 0;
};

#endif /* ASN1CPP_HPP */
///////////////////////////////////////
// integer definitions...            //
///////////////////////////////////////
#ifdef _INTEGER_H_
#ifndef _INTEGER_HPP_
#define _INTEGER_HPP_

/**
 * \class Integer
 * \brief Implementing IntegerBase, when asn1 INTEGER type is encoded with INTEGER_t
 */
class Integer: public Wrapper<INTEGER_t>, IntegerBase
{
public:
    /**
     * \brief constructor from internal INTEGER_t type
     * \param t data around what object is created
     * \param desc rules for encoding/decoding/constraint checking/freeing/...
     */
    Integer(INTEGER_t* t, asn_TYPE_descriptor_t* desc=&asn_DEF_INTEGER) :
        Wrapper<INTEGER_t>(t, desc)
    {
    }

    /**
     * \brief constructor for new object - should usually be used
     */
    Integer() :
        Wrapper<INTEGER_t>(0, &asn_DEF_INTEGER)
    {
    }

    /**
     * \brief getter for internal data
     * \returns internal data as long
     */
    long get()
    {
        fill_if_empty();
        long ret;
        asn_INTEGER2long(this->internal(), &ret);
        return ret;
    }

    /**
     * \brief getter for internal data
     * \returns internal data as unsigned long
     */
    unsigned long get_unsigned()
    {
        fill_if_empty();
        unsigned long ret;
        asn_INTEGER2ulong(this->internal(), &ret);
        return ret;
    }

    /**
     * \brief setter for internal data
     * \param l new internal value as long
     */
    void set(long l)
    {
        fill_if_empty();
        asn_long2INTEGER(internal(), l);
    }

    /**
     * \brief setter for internal data
     * \param l new internal value as unsigned long
     */
    void set_unsigned(unsigned long l)
    {
        fill_if_empty();
        asn_ulong2INTEGER(internal(), l);
    }
};

#endif /* INTEGER_HPP */
#endif /* INTEGER_H */
///////////////////////////////////////
// Explicit integer definitions...   //
///////////////////////////////////////
#ifdef _NativeInteger_H_
#ifndef _LONG_HPP_
#define _LONG_HPP_

/**
 * \class Long
 * \brief Implementing IntegerBase, when asn1 INTEGER type is encoded with long
 */
class Long: public Wrapper<long>, IntegerBase
{
public:
    /**
     * \brief constructor from internal INTEGER_t type
     * \param t data around what object is created
     * \param desc rules for encoding/decoding/constraint checking/freeing/...
     */
    Long(long* t, asn_TYPE_descriptor_t* desc=&asn_DEF_NativeInteger) :
        Wrapper<long>(t, desc)
    {
    }

    /**
     * \brief constructor for new object - should usually be used
     */
    Long() :
        Wrapper<long>(0, &asn_DEF_NativeInteger)
    {
    }

    /**
     * \brief getter for internal data
     * \returns internal data as long
     */
    long get()
    {
        fill_if_empty();
        return *internal();
    }

    /**
     * \brief getter for internal data
     * \returns internal data as unsigned long
     */
    unsigned long get_unsigned()
    {
        fill_if_empty();
        return *internal();
    }

    /**
     * \brief setter for internal data
     * \param l new internal value as long
     */
    void set(long l)
    {
        fill_if_empty();
        *internal() = l;
    }

    /**
     * \brief setter for internal data
     * \param l new internal value as unsigned long
     */
    void set_unsigned(unsigned long l)
    {
        fill_if_empty();
        *internal() = l;
    }
};

#endif /* _LONG_HPP_ */
#endif /* _NativeInteger_H_ */
///////////////////////////////////////
// NULL definitions...               //
///////////////////////////////////////
#if defined(_SimpleNull_H_) || defined(ASN_TYPE_NULL_H)
#ifndef _NULL_HPP_
#define _NULL_HPP_

/**
 * \class Null
 * \brief Implementing object for asn1 type NULL
 */
class Null: public Wrapper<NULL_t>
{
public:
    /**
     * \brief Constructor
     * \param t data around what object is created
     * \param desc rules for encoding/decoding/constraint checking/freeing/...
     */
    Null(NULL_t* t, asn_TYPE_descriptor_t* desc=&asn_DEF_NULL) :
        Wrapper<NULL_t>(t, desc)
    {
    }

    /**
     * \brief Constructor for new empty NULL object
     */
    Null() :
        Wrapper<NULL_t>(0, &asn_DEF_NULL)
    {
    }
};

#endif /* _NULL_HPP_ */
#endif /* _SimpleNull_H_ or ASN_TYPE_NULL_H */
///////////////////////////////////////
// Generalized type definitions...   //
///////////////////////////////////////
#ifdef _GeneralizedTime_H_
#ifndef _GENERALIZED_TIME_HPP_
#define _GENERALIZED_TIME_HPP_

/**
 * \class GeneralizedTime
 * \brief Implementing object for asn1 type GeneralizedTime
 */
class GeneralizedTime: public Wrapper<GeneralizedTime_t>
{
public:
    /**
     * \brief Constructor
     * \param t data around what object is created
     * \param desc rules for encoding/decoding/constraint checking/freeing/...
     */
    GeneralizedTime(GeneralizedTime_t* t, asn_TYPE_descriptor_t* desc=&asn_DEF_GeneralizedTime) :
        Wrapper<GeneralizedTime_t>(t, desc)
    {
    }

    /**
     * \brief Constructor for new empty GeneralizedTime object
     */
    GeneralizedTime() :
        Wrapper<GeneralizedTime_t>(0, &asn_DEF_GeneralizedTime)
    {
    }

    /**
     * \brief getter for internal data as time_t
     * \returns internal data as time_t
     * \param fraction if given, it is filled with second fraction
     * \param as_gmt decodes type as GMT time; default false
     */
    time_t get(double* fraction = NULL, bool as_gmt=false)
    {
        fill_if_empty();
        int fraction_value, fraction_digits;
        time_t ret = asn_GT2time_frac(internal(), &fraction_value, &fraction_digits, NULL, as_gmt);

        if(ret < 0) {
            throw std::runtime_error("Error decoding time.");
        }

        if(fraction != NULL) {
            *fraction = (double)fraction_value / (pow(10, fraction_digits));
        }
        return ret;
    }

    /**
     * \brief setter for internal data
     * \param time time in seconds, that is set
     * \param fraction of second, TO be written; default 0
     * \param as_gmt encodes type as GMT time; default false
     * \param fraction_digits determines how many digits in second fraction; default 3
     */
    void set(std::time_t time, double fraction=0, bool as_gmt=false, size_t fraction_digits=3)
    {
        fill_if_empty();
        int fraction_value = 0;
        if(fraction) {
            if(fraction < 0 || fraction >= 1) {
                throw std::runtime_error("Error encoding time: bad fraction.");
            }
            fraction_value = round(fraction * pow(10, fraction_digits));
        }

        struct tm* local = localtime(&time);
        asn_time2GT_frac(internal(), local, fraction_value, fraction_digits, as_gmt);

        if(internal() == 0) {
            _is_empty = true;
            throw std::runtime_error("Error encoding time.");
        }
    }

    /**
     * \brief setts internal data to current time
     * \param as_gmt encodes type as GMT time; default false
     * \param fraction_digits determines how many digits in second fraction; default 3
     *
     * currently unix specific!
     */
    void set_current_time(bool as_gmt=false, size_t fraction_digits=3)
    {
        fill_if_empty();
        struct timeval tv;
        time_t current_time;

        gettimeofday(&tv, NULL);
        current_time = tv.tv_sec;

        time_t local = mktime(localtime(&current_time));
        double ms = tv.tv_usec / 10e6;
        set(local, ms, as_gmt, fraction_digits);
    }
};

#endif /* _GeneralizedTime_H_ */
#endif /* _GeneralizedTime_HPP_ */
///////////////////////////////////////
// String definitions...             //
///////////////////////////////////////
#ifdef _OCTET_STRING_H_
#ifndef _UPPER_STRING_HPP_
#define _UPPER_STRING_HPP_

/**
 * \class UpperString
 * \brief Implementing object for asn1 string types
 */
template<typename STR_T>
class UpperString: public Wrapper<STR_T>
{
public:
    /**
     * \brief Constructor
     * \param t data around what object is created
     * \param desc rules for encoding/decoding/constraint checking/freeing/...
     */
    UpperString(STR_T* t, asn_TYPE_descriptor_t* desc) :
        Wrapper<STR_T>(t, desc)
    {
    }

    /**
     * \brief getter for internal data
     * \returns std::string representation of internal data
     */
    std::string get()
    {
        Wrapper<STR_T>::fill_if_empty();
        STR_T* intern = Wrapper<STR_T>::internal();

        const char* buf = (char*) (intern->buf);
        size_t size = intern->size;

        std::string s(buf, size);
        return s;
    }

    /**
     * \brief setter for internal data
     * \returns param s string that is written to internal data
     */
    void set(std::string s)
    {
        Wrapper<STR_T>::fill_if_empty();
        OCTET_STRING_fromBuf(Wrapper<STR_T>::internal(), s.c_str(), s.length());
    }
};

#endif /* _UPPER_STRING_HPP_ */
#endif /* _OCTET_STRING_H_ */
///////////////////////////////////////
// Boolean definitions..             //
///////////////////////////////////////
#ifdef _BOOLEAN_H_
#ifndef _BOOLEAN_HPP_
#define _BOOLEAN_HPP_

/**
 * \class Boolean
 * \brief Implementing object for asn1 type BOOLEAN
 */
class Boolean: public Wrapper<BOOLEAN_t>
{
public:
    /**
     * \brief Constructor
     * \param t data around what object is created
     */
    Boolean(BOOLEAN_t* t) :
        Wrapper<BOOLEAN_t>(t, &asn_DEF_BOOLEAN)
    {
    }

    /**
     * \brief Constructor for new empty Bool object
     */
    Boolean() :
        Wrapper<BOOLEAN_t>(0, &asn_DEF_BOOLEAN)
    {
    }

    /**
     * \brief getter for internal data
     * \returns internal data as bool
     */
    bool get()
    {
        fill_if_empty();
        return *internal();
    }

    /**
     * \brief setter for internal data
     * \param b sets internal data to this value
     */
    void set(bool b)
    {
        fill_if_empty();
        *internal() = b;
    }
};

#endif /* _BOOLEAN_HPP_ */
#endif /* _BOOLEAN_H_ */
///////////////////////////////////////
// enum definitions                  //
///////////////////////////////////////
#if defined(_ENUMERATED_H_) || defined(_NativeEnumerated_H_)
#ifndef _ENUMERATED_HPP_
#define _ENUMERATED_HPP_

/**
 * \class Enum
 * \brief Is inherited by any ENUMERATED type
 */
template<typename T, typename ENUM_T, typename MY_T>
class Enum: public Wrapper<T>
{
public:
    /**
     * \brief Constructor
     * \param t data around what object is created
     * \param desc describes this enum type
     */
    Enum(T* t, asn_TYPE_descriptor_t* desc, ENUM_T default_enum) :
        Wrapper<T>(t, desc, _fill_enum), _default_enum(default_enum)
    {
    }

    /**
     * \brief getter for internal data, fills if empty
     * \return internal enum
     *
     * TODO: support -fwide-types in future
     */
    ENUM_T get()
    {
        Wrapper<T>::fill_if_empty();
        return (ENUM_T) *Wrapper<T>::internal();
    }

    /// another way to get internal value
    inline bool is(ENUM_T s)
    {
        return get() == s;
    }

    /**
     * \brief setter for internal data
     * \param s new enum value
     *
     * TODO: support -fwide-types in future
     */
    void set(ENUM_T s)
    {
        Wrapper<T>::fill_if_empty();
        *Wrapper<T>::internal() = s;
    }

    /// sets default value for enum
    void set_default()
    {
        if(!Wrapper<T>::is_empty()) {
            set(_default_enum);
        }
    }

private:
    /**
     * \brief special fill function for enum types
     *
     * First value in enum is chosen at initialisation.
     */
    static void _fill_enum(Wrapper<T>* w)
    {
        w->fill(true);
        ((MY_T*)w)->set_default();
    }

    /// default enum, initialized in constructor
    ENUM_T _default_enum;
};

#endif /* _ENUMERATED_HPP_ */
#endif /* _ENUMERATED_H_ or _NativeEnumerated_H_*/
///////////////////////////////////////
// bit string definition             //
///////////////////////////////////////
#ifdef _BIT_STRING_H_
#ifndef _BIT_STRING_HPP_
#define _BIT_STRING_HPP_

/**
 * \class BitString
 * \brief is inherited by any BIT STRING type
 */
template<typename BIT_T, typename E_BIT, size_t SIZE>
class BitString: public Wrapper<BIT_T>
{
public:
    /**
     * \brief Constructor
     * \param t data around what object is created
     * \param desc describes this bit string type
     */
    BitString(BIT_T* t, asn_TYPE_descriptor_t* desc) :
        Wrapper<BIT_T>(t, desc, _fill_bit_string)
    {
    }

    ///brief public interface to _get_bit
    inline bool is(E_BIT b)
    {
        return _get_bit(b);
    }

    ///brief public interface to _set_bit
    inline void set(E_BIT b, bool v)
    {
        _set_bit(b, v);
    }

protected:
    /**
     * \brief setter for internal data
     * \param loc locations in bitstring, which to set/unset
     * \param new value of bit at location loc
     *
     * Did not find implemented in asn1c, so implemented here by hand
     */
    void _set_bit(uint8_t loc, bool value)
    {
        Wrapper<BIT_T>::fill_if_empty();
        uint8_t* set_at = &(Wrapper<BIT_T>::internal()->buf[loc / 8]);
        uint8_t set_bit_at = (7 - (loc % 8));
        if (value) {
            uint8_t setter = 1 << set_bit_at;
            *set_at |= setter;
        } else {
            uint8_t setter = (1 << set_bit_at) ^ 0xFF;
            *set_at &= setter;
        }
    }

    /**
     * \brief getter for internal data
     * \param loc locations in bitstring, which to return
     * \returns value of bit at location loc
     *
     * Did not find implemented in asn1c, so implemented here by hand
     */
    bool _get_bit(uint8_t loc)
    {
        Wrapper<BIT_T>::fill_if_empty();
        uint8_t* get_at = &(Wrapper<BIT_T>::internal()->buf[loc / 8]); // byte location
        uint8_t get_bit_at = 1 << (7 - (loc % 8)); // bit location
        return *get_at & get_bit_at;
    }
private:

    /**
     * \brief special fill function for bitstring types
     *
     * I could not find this implemented inside ans1c, so this was implemented by hand
     */
    static void _fill_bit_string(Wrapper<BIT_T>* w)
    {
        w->fill(true);

        uint bytes = (SIZE - 1) / 8 + 1;
        w->internal()->buf = (uint8_t*) calloc(bytes, sizeof(uint8_t));
        w->internal()->size = bytes;

        // guaranteed above
        assert(bytes * 8 - SIZE <= 7);
        w->internal()->bits_unused = bytes * 8 - SIZE;
    }
};

#endif /* _BIT_STRING_HPP_ */
#endif /* _BIT_STRING_H_ */
///////////////////////////////////////
// Choice definitions...             //
///////////////////////////////////////
#ifndef _CHOICE_HPP_
#define _CHOICE_HPP_

/**
 * \class Choice
 * \brief is inherited by any CHOICE type
 */
template<typename CHOICE_T, typename e_CHOICE>
class Choice: public Wrapper<CHOICE_T>
{
public:
    /**
     * \brief Constructor
     * \param t data around what object is created
     * \param desc describes this choice type
     */
    Choice(CHOICE_T* t, asn_TYPE_descriptor_t* desc) :
        Wrapper<CHOICE_T>(t, desc)
    {
    }

    /**
     * \brief getter for type of internal value
     * \returns chosen value in CHOICE
     */
    e_CHOICE which()
    {
        Wrapper<CHOICE_T>::fill_if_empty();
        return Wrapper<CHOICE_T>::internal()->present;
    }

    /**
     * \brief setter for type internal data
     * \param t sets type of internal data
     */
    void set(e_CHOICE t)
    {
        Wrapper<CHOICE_T>::fill_if_empty();
        Wrapper<CHOICE_T>::internal()->present = t;
        std::memset(&Wrapper<CHOICE_T>::internal()->choice, 0,
                    sizeof(Wrapper<CHOICE_T>::internal()->choice));
    }

    /**
     * \brief getter for type of internal value
     * \param t true is type is chosen
     * \returns true is type of internal value is t
     */
    bool is(e_CHOICE t)
    {
        Wrapper<CHOICE_T>::fill_if_empty();
        return which() == t;
    }

protected:
    /**
     * \brief checks if internal type is t
     *
     * if internal type not t, then throw runtime_error
     */
    void _check_type(e_CHOICE t)
    {
        if (!is(t)) {
            assert(t > 0);
            asn_TYPE_member_t* member = (asn_TYPE_member_t*)(Wrapper<CHOICE_T>::description()->elements);
            const char* choice = member[t - 1].name; // skipping PR_NOTHING in enum
            std::stringstream ss;
            ss << "Checking choice '" << choice << "' inside '" << Wrapper<CHOICE_T>::type() << "' failed";
            throw std::runtime_error(ss.str());
        }
    }
};

#endif /* _CHOICE_HPP_ */
///////////////////////////////////////
// Choice definitions...             //
///////////////////////////////////////
#ifndef _SEQUENCE_OF_HPP_
#define _SEQUENCE_OF_HPP_
template <typename T, typename ITEM_T>

/**
 * \class SequenceOf
 * \brief is inherited by any SEQUENCE OF type
 *
 * TODO: fix leakage
 */
class SequenceOf: public Wrapper<T>
{
public:
    /**
     * \brief Constructor
     * \param t data around what object is created
     * \param desc describes this sequence of type
     */
    SequenceOf(T* t, asn_TYPE_descriptor_t* desc) :
        Wrapper<T>(t, desc)
    {
    }

    /**
     * \brief getter for size of array
     */
    size_t size()
    {
        Wrapper<T>::fill_if_empty();
        return Wrapper<T>::internal()->list.count;
    }

    /**
     * \brief array getter
     * \param t idx index in array to get
     */
    ITEM_T operator[](size_t idx)
    {
        Wrapper<T>::fill_if_empty();
        throw_on_range(idx);
        ITEM_T item = Wrapper<T>::internal()->list.array[idx];
        item.set_phony_parent();

        return item;
    }

    /**
     * \brief delete array in item
     * \param idx index of element in array to delete
     * \param free flag to enable/disable freeing
     */
    void del(size_t idx, bool free=true)
    {
        Wrapper<T>::fill_if_empty();
        throw_on_range(idx);
        asn_set_del(Wrapper<T>::internal(), idx, free);
    }

    /**
     * \brief pushes an item to array
     * \param el element which to push
     */
    void add(ITEM_T& el)
    {
        Wrapper<T>::fill_if_empty();
        if(el.has_parent()) {
            // creating copy if parent is already set
            ITEM_T el_copy(el);
            el_copy.set_phony_parent();
            asn_set_add(Wrapper<T>::internal(), el_copy.internal());
        } else {
            // else adding parent and not copying
            asn_set_add(Wrapper<T>::internal(), el.internal());
            el.set_phony_parent();
        }
    }

    /**
     * \brief removes all elements from array
     */
    void clear()
    {
        Wrapper<T>::fill_if_empty();
        asn_set_empty(Wrapper<T>::internal());
    }

protected:
    /**
     * \brief checks if idx over size
     * \param idx throws if idx > size()
     */
    void throw_on_range(size_t idx)
    {
        if(idx >= size()) {
            throw std::range_error("Index outside range");
        }
    }
};

#endif /* _SEQUENCE_OF_HPP_ */
///////////////////////////////////////
// Basic String types                //
///////////////////////////////////////
#ifdef _IA5String_H_
#ifndef _IA5String_HPP_
#define _IA5String_HPP_

/**
 * \class IA5String
 * \brief implementation of UpperString for IA5String
 */
class IA5String: public UpperString<IA5String_t>
{
public:
    /**
     * \brief Constructor
     * \param t data around what object is created
     * \param desc describes this sequence of type
     */
    IA5String(IA5String_t* t, asn_TYPE_descriptor_t* desc=&asn_DEF_IA5String) :
        UpperString<IA5String_t>(t, desc)
    {
    }

    /**
     * \brief Constructor for new empty IA5String object
     */
    IA5String() :
        UpperString<IA5String_t>((IA5String_t*) 0, &asn_DEF_IA5String)
    {
    }
};

#endif /* _IA5String_HPP_ */
#endif /* _IA5String_H_ */

#ifdef _OCTET_STRING_H_
#ifndef _OCTET_STRING_HPP_
#define _OCTET_STRING_HPP_

/**
 * \class OctetString
 * \brief implementation of UpperString for OCTET_STRING
 */
class OctetString: public UpperString<OCTET_STRING_t>
{
public:
    /**
     * \brief Constructor
     * \param t data around what object is created
     * \param desc describes this sequence of type
     */
    OctetString(OCTET_STRING_t* t, asn_TYPE_descriptor_t* desc=&asn_DEF_OCTET_STRING) :
        UpperString<OCTET_STRING_t>(t, desc)
    {
    }

    /**
     * \brief Constructor for new empty OCTET_STRING object
     */
    OctetString() :
        UpperString<OCTET_STRING_t>((OCTET_STRING_t*) 0, &asn_DEF_OCTET_STRING)
    {
    }
};

#endif /* _OCTET_STRING_HPP_ */
#endif /* _OCTET_STRING_H_ */

} /* end asn1 namespace */
