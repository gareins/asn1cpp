import tempfile
import os
from collections import defaultdict
from subprocess import Popen, PIPE
import shutil
import sys
import re

from asn1_types import *

try:
    # noinspection PyUnresolvedReferences
    import pycparser
except ImportError as e:
    print("Cannot import pycparser. Is it installed??")
    sys.exit(1)
try:
    # noinspection PyUnresolvedReferences
    from pycparserext import ext_c_parser as gnu_parser
except ImportError as e:
    print("Cannot import pycparserext. Is it installed??")
    sys.exit(1)


class Asn1Error(Exception):
    def __init__(self, message, errors):
        super(Exception, self).__init__(message)
        self.errors = errors


class AsnCpp:
    def __init__(self, asn_files, asn_options, asn_cmd="asn1c", status_len=30, out_dir=os.getcwd(),
                 sys_headers=".", asn1_headers=".", asn1cpp_header="."):

        sys_hds = sys_headers
        asn_hds = asn1_headers

        self.out_dir = out_dir if out_dir.endswith("/") else out_dir + "/"
        self.sys_hds = sys_hds if sys_hds.endswith("/") else sys_hds + "/"
        self.asn_hds = asn_hds if asn_hds.endswith("/") else asn_hds + "/"

        self.asn_options = asn_options
        self.asn_files = asn_files
        self.asn_cmd = asn_cmd
        self.status_len = status_len
        self.asn1cpp_header = asn1cpp_header

        self._tmp_dir = tempfile.TemporaryDirectory(prefix="asn_")
        self.dir_1 = self.tmp_dir() + "/step1/"
        self.dir_2 = self.tmp_dir() + "/step2/"

        os.mkdir(self.dir_1)
        os.mkdir(self.dir_2)

    def tmp_dir(self):
        return self._tmp_dir.name

    def print_status(self, s):
        if False:
            print("\n" + "*" * self.status_len)
            fmt = "* {:" + str(self.status_len - 4) + "s} *"
            print(fmt.format(s))
            print("*" * self.status_len, flush=True)

    @staticmethod
    def strip_header(fin, fout):
        regex = re.compile(r"# \d+ .*")
        with open(fin, "r") as r, open(fout, "w") as w:
            txt = re.sub("\n\n+", "\n\n", r.read())

            for line in txt.split("\n"):
                if regex.search(line):
                    continue
                w.write(line + "\n")

    def generate_parsable_header(self, headers):
        regex = re.compile(r"#include\s+[\"<]([a-z0-9-_/]+\.h)[\">].*")
        with open("full_header_1.h", "w") as fht:
            for f in headers:
                fht.write("#include <" + self.dir_1 + f + ">\n")
                # replacing local #include "..." with include "..." (removing hashes)
                with open(self.dir_1 + f, "r") as r, open(f, "w") as w:
                    for line in r.readlines():
                        matches = regex.search(line.strip())
                        if matches and matches.group(1) not in headers:
                            line = line.strip()[1:]

                        w.write(line)

        cmd = ["cpp", "-std=c90", "-Dinclude=#include"] + \
              ["-I.", "-I" + self.asn_hds, "-I" + self.sys_hds] + \
              ["full_header_1.h", "-o", "full_header_2.h"]
        p = Popen(cmd, stderr=PIPE, close_fds=True)

        if p.wait() != 0:
            print(p.stderr.read().decode("utf-8"))
            self.print_status("ERR")
            raise Exception

    @staticmethod
    def collect_structs(asn_output):
        structs, internals = [], []

        for f in asn_output.split("\n"):
            if f.endswith("c"):
                continue
            if "Compiled" in f:
                structs.append(f.split(" ")[1])
            elif "Symlinked" in f:
                internals.append(f.split(" ")[2])
            else:
                continue

        return structs, internals

    def run_asn1c(self):
        cmd = [self.asn_cmd] + self.asn_options + self.asn_files
        p = Popen(cmd, stderr=PIPE, close_fds=True)

        if p.wait() != 0:
            raise Asn1Error("Asn1c runtime error", p.stderr.read().decode("utf-8"))

        return p.stderr.read().decode("utf-8")

    @staticmethod
    def parse_c_ast(ast, structs):
        types_enums = defaultdict(lambda: {"enum": None, "type": None})

        def is_type(x):
            return x.endswith("_t") and x[:-2] in structs

        def is_enum(x):
            return x.startswith("e_") and x[2:] in structs

        for module in ast.ext:
            if type(module) == pycparser.c_ast.Typedef:
                if not hasattr(module.type, "declname"):
                    continue

                declname = module.type.declname
                if is_type(declname):
                    types_enums[declname[:-2]]["type"] = module
                elif is_enum(declname):
                    types_enums[declname[2:]]["enum"] = module

        types = [AsnType.get_asn_type(te["type"], te["enum"]) for _, te in types_enums.items()]
        AsnType.get_real_parents(types)
        return types

    def generate_bindings(self, types):
        os.symlink(self.asn1cpp_header, "asn1cpp.hpp")
        try:
            os.remove("Makefile.am.sample")
            os.remove("converter-sample.c")
        except OSError:
            pass

        for t in types:
            bind = t.generate_cpp_bind()
            self.write_binding(t.name, bind)

    @staticmethod
    def write_binding(file, binding):
        # should be located at dir_1
        with open(file + ".h", "r") as fp:
            file_content = fp.readlines()
            file_before, file_after = file_content[:-3], file_content[-3:]

        with open(file + ".h", "w") as fp:
            fp.writelines(file_before)
            fp.write("\n//CXX bindings\n#ifdef __cplusplus\n\n")

            fp.write('#include "asn1cpp.hpp"\n')
            fp.write(binding)

            fp.write("\n\n#endif\n")
            fp.writelines(file_after)

    def copy_out(self):
        # stackoverflow
        def copytree(src, dst, symlinks=False, ignore=None):
            for item in os.listdir(src):
                s = os.path.join(src, item)
                d = os.path.join(dst, item)
                if os.path.isdir(s):
                    shutil.copytree(s, d, symlinks, ignore)
                else:
                    shutil.copy2(s, d)

        copytree(os.path.curdir, self.out_dir)

    def work(self):
        self.print_status("Compiling asn files")
        os.chdir(self.dir_1)
        asn_out = self.run_asn1c()

        self.print_status("Collecting compiled files")
        structs, internals = self.collect_structs(asn_out)
        headers = structs + internals

        self.print_status("Generating header")
        os.chdir(self.dir_2)
        self.generate_parsable_header(headers)

        self.print_status("Stripping header")
        self.strip_header("full_header_2.h", "full_header_3.h")

        self.print_status("Moving generated files")
        shutil.copy("full_header_3.h", self.out_dir + "asn_internal.h")

        self.print_status("Parse header")
        with open("full_header_3.h") as fp:
            parser = gnu_parser.GnuCParser()
            ast = parser.parse(fp.read(), filename='<none>')
            types = self.parse_c_ast(ast, [s[:-2] for s in structs])

        self.print_status("Generate bindings")
        os.chdir(self.dir_1)
        self.generate_bindings(types)

        # TODO: if compiling

        self.print_status("Overwrite and end")
        self.copy_out()

        # self.print_status("Compile library")
        # gcc compile to shared library

# ([a-zA-Z][a-zA-Z0-9\-]+)[\ \t]+([a-zA-Z]+)\s+::=[\ \t]+(.*+)
