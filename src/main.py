#!/usr/bin/python3

from sys import argv, exit, stderr
import os
import re

from __init__ import __version__ as version
from compiler import AsnCpp, Asn1Error
import subprocess


def print_help():
    pipe = subprocess.Popen(["asn1c", "-h"], stderr=subprocess.PIPE)
    help_asn1c = pipe.stderr.read().decode("ascii").split("\n")

    orig_version = help_asn1c[0].split(" ")[-1]
    help_msg = """asn1cpp compiler v{} (using asn1c {})
Copyright (c) 2016 Ožbolt Menegatti
Usage: asn1cpp [CPP/C options] asn1_files*
CPP options:
  -CXX                  Enable Cxx bindings
  -O <dir>              Directory where *.c/h files will be compiled to. Default: ./
  -Wdebug-CXX           Verbose debugging for CXX compiler

C options:\n""".format(version, orig_version)

    help_msg += "\n".join(help_asn1c[5:])
    print(help_msg)


def parse_dir(itr, idx, trm):
    if len(argv) == idx + 1:
        print("Missing after {}".format(argv[i]), file=stderr)
        exit(1)
    elif not os.path.isdir(argv[idx + 1]):
        print("Not a directory: {}".format(argv[i + 1]), file=stderr)
        exit(1)
    else:
        trm += [idx, idx + 1]
        next(itr)
        return os.path.abspath(argv[idx + 1])

if __name__ == "__main__":
    flags = {
        "conflict": r"\-(E|F|P|X|fwide\-types)",
        "untested": r"\-(fbless\-SIZE|findirect\-choice|fincludes\-quoted|" +
                    r"fknown\-extern\-type=([0-9a-zA-Z\-]+)|fline\-refs|" +
                    r"fno\-include\-deps|gen-PER|pdu=([0-9a-zA-Z\-]+)|" +
                    r"print\-class\-matrix|print\-constraints|print\-lines|funnamed\-unions)",
        "pass": r"\-(R|Wdebug\-lexer|Wdebug\-fixer|Wdebug\-compiler|fcompound\-names)",
        "cxx_debug": r"\-Wdebug\-CXX",
        "err": r"\-Werror",
        "constraints": r"\-fno\-constraints",
        "cpp": r"\-CXX",
        "asn1_dir": r"\-S",
        "dir": r"\-O",
        "help": r"\-h",
    }

    conflict, out_dir = None, '.'
    constraints, cpp_enable = False, False
    cxx_debug, err = False, False
    last_flag, cxx_only_flag = 0, None
    asn1_dir = "/usr/local/share/asn1c"

    untested, to_remove = [], [0]
    iterator = enumerate(argv)
    next(iterator)  # skip first

    for i, arg in iterator:
        if re.match(flags["conflict"], arg):
            conflict = arg

        elif re.match(flags["untested"], arg):
            untested.append(arg)

        elif re.match(flags["pass"], arg):
            pass

        elif re.match(flags["cxx_debug"], arg):
            cxx_debug = True
            cxx_only_flag = arg
            to_remove += i

        elif re.match(flags["constraints"], arg):
            constraints = True

        elif re.match(flags["cpp"], arg):
            to_remove += [i]
            cpp_enable = True

        elif re.match(flags["err"], arg):
            err = True

        elif re.match(flags["asn1_dir"], arg):
            asn1_dir = parse_dir(iterator, i, to_remove)
            i += 1

        elif re.match(flags["dir"], arg):
            out_dir = parse_dir(iterator, i, to_remove)
            i += 1

        elif re.match(flags["help"], arg):
            print_help()
            exit(0)            

        elif arg.startswith("-"):
            print("Unknown flag: {}. Try -h for help".format(arg), file=stderr)
            exit(1)

        else:
            continue

        last_flag = i

    files = [os.path.abspath(p) for p in argv[last_flag + 1:]]
    argv_asn1c = [x for i, x in enumerate(argv[:last_flag + 1]) if i not in to_remove]

    # just pass to asn1c
    if not cpp_enable:
        if cxx_only_flag is not None:
            print("CXX option {} without -CPP flag is not allowed!".format(cxx_only_flag), file=stderr)
            exit(1)
        print("", end="", flush=True)

        os.chdir(out_dir)
        cmd = ["asn1c"] + argv_asn1c + files
        ret = subprocess.call(cmd)
        exit(ret)

    elif conflict is not None:
        print("Flag {} cannot be set alongside -CPP!".format(conflict), file=stderr)
        exit(1)

    # order:
    # TODO: check dependencies
    # TODO: support -Werror, cxx_debug
    # TODO: get asn1 constants
    # TODO: option to compile to static lib and only output headers

    if len(untested) > 0:
        print("Flags [{}] are not yet tested with CPP!".format(", ".join(untested)), file=stderr)

    module_path = os.path.abspath(os.path.dirname(__file__))
    out_dir = os.path.abspath(out_dir)

    fake_libc_includes = module_path + "/fake_libc_include"
    if not os.path.isdir(fake_libc_includes):
        print("Cannot find fake_libc_include folder, expected here: {}!".format(fake_libc_includes), file=stderr)
        exit(1)

    asn1cpp_header = module_path + "/asn1cpp.hpp"
    if not os.path.isfile(asn1cpp_header):
        print("Cannot find asn1cpp.hpp file, expected here: {}!".format(asn1cpp_header), file=stderr)
        exit(1)

    if not os.path.isdir(out_dir):
        print("Cannot find output directory, expected here: {}!".format(out_dir), file=stderr)
        exit(1)

    try:
        asn = AsnCpp(files, argv_asn1c, out_dir=out_dir, asn1_headers=asn1_dir,
                     sys_headers=fake_libc_includes, asn1cpp_header=asn1cpp_header)
        asn.work()
    except Asn1Error as e:
        print("Error: " + str(e), file=stderr)
        print(e.errors, file=stderr)
        exit(1)

    exit(0)
