#include "_asn1/LongInteger.h"
#include "_asn1/SimpleInteger.h"
#include "_asn1/SimpleEnum.h"
#include "_asn1/BitStringSimple.h"
#include "_asn1/ReversedBS.h"
#include "_asn1/SimpleNull.h"
#include "_asn1/MyTime.h"

class Basic : public ::testing::Test {};


TEST_F(Basic, EnumDecoderEncoder) {
	std::vector<uint8_t> et_ber(3);
	et_ber[0] = 0x0A;
	et_ber[1] = 0x01;
	et_ber[2] = 0x06;

	asn1::SimpleEnum et;
	et.from_ber(&et_ber[0], et_ber.size());

	ASSERT_STREQ("<SimpleEnum><fourth/></SimpleEnum>\n", et.to_xer().c_str());
	et.set(SimpleEnum_second);
	ASSERT_STREQ("<SimpleEnum><second/></SimpleEnum>\n", et.to_xer().c_str());
	et.set(SimpleEnum_first);
	ASSERT_STREQ("<SimpleEnum><first/></SimpleEnum>\n", et.to_xer().c_str());

	// first by default
	asn1::SimpleEnum et2;
	et2.fill();
	ASSERT_STREQ(et.to_xer().c_str(), et2.to_xer().c_str());

	et2.set(SimpleEnum_fourth);
	std::vector<uint8_t> et2_ber = et2.to_ber();
	EXPECT_ARRAY_EQ(et_ber, et2_ber);
}

TEST_F(Basic, XER_BER_enum) {
    asn1::SimpleNull sn;
    const char* xer_str = "<SimpleNull></SimpleNull>\n";

    // BER
    std::vector<uint8_t> sn_ber(2);
    sn_ber[0] = 0x05;
    sn_ber[1] = 0x00;
    sn.decode(&sn_ber[0], 2, asn1::BER);
    ASSERT_STREQ(xer_str, sn.to_xer().c_str());
	
    // XER
    sn.decode(&xer_str[0], strlen(xer_str), asn1::XER);
    EXPECT_ARRAY_EQ(sn_ber, sn.to_ber());
}

TEST_F(Basic, IntegerWithConstraints) {
	asn1::SimpleInteger si;
	ASSERT_THROW(si.to_xer(), std::runtime_error);
	si.fill();

	si.set(123123124);
	ASSERT_TRUE(!si.check_constraints());
	si.to_ber();
	ASSERT_TRUE(!si.check_constraints());
	ASSERT_STREQ("<SimpleInteger>123123124</SimpleInteger>\n", si.to_xer().c_str());
	ASSERT_TRUE(!si.check_constraints());

	std::string constraints_check;
	si.check_constraints(constraints_check);
	constraints_check = constraints_check.substr(0, 61);
	const char* chk = constraints_check.c_str();
	ASSERT_STREQ("SimpleInteger: constraint failed (basic/_asn1/SimpleInteger.c", chk);

	asn1::SimpleInteger si2;
	si2.set(75);
	ASSERT_TRUE(si2.check_constraints());
	ASSERT_STREQ("<SimpleInteger>75</SimpleInteger>\n", si2.to_xer().c_str());
	ASSERT_TRUE(si2.check_constraints());

	asn1::LongInteger lng;
	lng.set_unsigned(123123124);
	ASSERT_TRUE(lng.check_constraints());
	ASSERT_STREQ("<LongInteger>123123124</LongInteger>\n", lng.to_xer().c_str());

	lng.set(75);
	ASSERT_EQ(lng.get_unsigned(), si2.get_unsigned());
}

TEST_F(Basic, BitStringDecoderEncoder) {
	std::vector<uint8_t> et_ber(4);
	et_ber[0] = 0x03;
	et_ber[1] = 0x02;
	et_ber[2] = 0x04;
	et_ber[3] = 0x60;

	asn1::BitStringSimple et;
	et.from_ber(&et_ber[0], et_ber.size());

	ASSERT_STREQ("<BitStringSimple>\n    0110\n</BitStringSimple>\n", et.to_xer().c_str());
	et.set(BitStringSimple_foo, true);
	ASSERT_STREQ("<BitStringSimple>\n    1110\n</BitStringSimple>\n", et.to_xer().c_str());
	et.set(BitStringSimple_foo, false);
	ASSERT_STREQ("<BitStringSimple>\n    0110\n</BitStringSimple>\n", et.to_xer().c_str());

	ASSERT_TRUE(et.is(BitStringSimple_lorem));
	ASSERT_FALSE(et.is(BitStringSimple_ipsum));

	asn1::BitStringSimple et2;
	et2.set(BitStringSimple_bar, true);
	et2.set(BitStringSimple_lorem, true);

	ASSERT_STREQ(et.to_xer().c_str(), et2.to_xer().c_str());
	std::vector<uint8_t> et2_ber = et2.to_ber();
	EXPECT_ARRAY_EQ(et_ber, et2_ber);
}

TEST_F(Basic, BitStringStrangeOrder) {
	asn1::ReversedBS et;
	et.fill();
	ASSERT_STREQ("<ReversedBS>\n    00000\n</ReversedBS>\n", et.to_xer().c_str());

	et.set(ReversedBS_foo, true);
	ASSERT_STREQ("<ReversedBS>\n    00001\n</ReversedBS>\n", et.to_xer().c_str());

	et.set(ReversedBS_bar, true);
	ASSERT_STREQ("<ReversedBS>\n    10001\n</ReversedBS>\n", et.to_xer().c_str());
}

TEST_F(Basic, NullDecoderEncoder) {
	std::vector<uint8_t> sn_ber(2);
	sn_ber[0] = 0x05;
	sn_ber[1] = 0x00;

	asn1::SimpleNull sn;
	sn.fill();

	ASSERT_STREQ("<SimpleNull></SimpleNull>\n", sn.to_xer().c_str());
	EXPECT_ARRAY_EQ(sn_ber, sn.to_ber());

	sn.from_ber(&sn_ber[0], sn_ber.size());
	ASSERT_STREQ("<SimpleNull></SimpleNull>\n", sn.to_xer().c_str());
}

TEST_F(Basic, GeneralizedTimeStuff) {
	asn1::MyTime myt;

	struct tm t;
    t.tm_sec = 12;
    t.tm_min = 51;
    t.tm_hour = 8;
    t.tm_mday = 27;
    t.tm_mon = 9;
    t.tm_year = 2016 - 1900;
    t.tm_wday = 3;
    t.tm_yday = 300;
    t.tm_isdst = 1;

    time_t local = mktime(&t);
    myt.set(local);
	ASSERT_STREQ("<MyTime>20161027085112+0200</MyTime>\n", myt.to_xer().c_str());
    myt.set(local, 0, true);
	ASSERT_STREQ("<MyTime>20161027065112Z</MyTime>\n", myt.to_xer().c_str());

	double frac = 0.12564;
	myt.set(local, frac);
	ASSERT_STREQ("<MyTime>20161027085112.126+0200</MyTime>\n", myt.to_xer().c_str());

	double frac2;
	time_t t2 = myt.get(&frac2);
	double diff = difftime(mktime(&t), t2);

	ASSERT_EQ(diff, 0);
	ASSERT_EQ(frac2, 0.126);

	asn1::MyTime myt3;
	myt3.set_current_time();
	time_t t3 = myt3.get();

	ASSERT_LT(difftime(mktime(&t), t3), 0);
}

TEST_F(Basic, DecodeVsFrom_) {
	asn1::MyTime myt;
	myt.set_current_time();

	std::string xer_str = myt.to_xer();
	const void* xdata = &xer_str[0];

    asn1::MyTime t1, t2;
    ASSERT_TRUE(t1.from_xer(xer_str));
    ASSERT_TRUE(t2.decode(xdata, xer_str.length(), asn1::XER));

	ASSERT_STREQ(t1.to_xer().c_str(), t2.to_xer().c_str());
}

TEST_F(Basic, CopySimpleEnum) {
    asn1::SimpleEnum ss;
    asn1::SimpleEnum ss2(ss);
    ss2.fill();
    asn1::SimpleEnum ss3(ss2);
}

