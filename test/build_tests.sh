#!/bin/bash

cd $(dirname $0)

echo "Building asn1cpp for every test"
dirs=$(find . -maxdepth 1 -mindepth 1 -type d -printf '%f\n')
asn1c_share="/usr/local/share/asn1c"

hash premake5 || exit 1

if [ ! -d $asn1c_share ]; then
  echo "$asn1c_share is not a directory..."
  exit 1
else
  rm -r asn1c_headers 2>/dev/null
  cp -r $asn1c_share $PWD/asn1c_headers
  rm $PWD/asn1c_headers/converter-sample.c
fi

for d in $dirs
do
  if [ ! -f $d/cmd ]; then
    echo "No cmd in $d, skipping..."
    continue
  fi
  rm -r $d/_asn1 2>/dev/null
  mkdir $d/_asn1
  
  cd $d  
  
  cmd=$(head -n 1 cmd)
  files=$(ls | grep "\.asn$")
  cmd="../../src/main.py $cmd -R -O _asn1 $files"

  echo "compiling $d : $cmd"
  $cmd || exit 1
  cd ..
done

echo "" &&
premake5 gmake &&
echo "" &&
echo "Run make && ./build/Asn1CppTest"


