#include "_asn1/SimpleSequence.h"
#include "_asn1/ExampleChoice.h"
#include "_asn1/SomeSequence.h"
#include "_asn1/NotSoSS.h"
#include "_asn1/Reserved.h"
#include "_asn1/OneOptional.h"
#include "_asn1/SeqEnum.h"
#include "_asn1/Seq2Enum.h"

class CompoundBasic : public ::testing::Test {};

TEST_F(CompoundBasic, SequenceOfDecoderEncoder) {
	asn1::SimpleSequence ss;
	ASSERT_THROW(ss.to_xer(), std::runtime_error);

	ss.fill();
	std::string xer = ss.to_xer();
	ASSERT_STREQ(
			"<SimpleSequence>\n"
			"</SimpleSequence>\n",
			xer.c_str());

	asn1::Long lng;
	lng.set(10);
	ss.add(lng);

	xer = ss.to_xer();
	ASSERT_STREQ(
			"<SimpleSequence>\n"
			"    <INTEGER>10</INTEGER>\n"
			"</SimpleSequence>\n",
			xer.c_str());

	asn1::Long lng_back = ss[0];
	ASSERT_STREQ(lng.to_xer().c_str(), lng_back.to_xer().c_str());

	ASSERT_THROW(ss[1], std::range_error);
	ASSERT_EQ(ss.size(), 1);

	ss.del(0);
	ss.add(lng);
	ss.add(lng_back);
	ASSERT_EQ(ss.size(), 2);
	ss.clear();
	ASSERT_EQ(ss.size(), 0);
}


TEST_F(CompoundBasic, SequenceOfDuplicatedFree) {
	asn1::SimpleSequence ss;
	asn1::Long lng;
	lng.set(10);

	ss.add(lng);
	ss.add(lng);
	// this will segfault if add() or _sp() inside add() does something wrong!
}

TEST_F(CompoundBasic, SequenceOfSequenceOf) {
	asn1::NotSoSS nsss;
	ASSERT_THROW(nsss.to_xer(), std::runtime_error);
	nsss.fill();

	ASSERT_STREQ(
			"<NotSoSS>\n"
			"</NotSoSS>\n",
			nsss.to_xer().c_str());

	asn1::SimpleSequence ss;
	for(uint i = 0; i < 5; i++)
	{
		asn1::Long lng;
		lng.set(i + 13);
		ss.add(lng);
	}
	nsss.add(ss);

	ASSERT_STREQ(
			"<NotSoSS>\n"
			"    <SimpleSequence>\n"
			"        <INTEGER>13</INTEGER>\n"
			"        <INTEGER>14</INTEGER>\n"
			"        <INTEGER>15</INTEGER>\n"
			"        <INTEGER>16</INTEGER>\n"
			"        <INTEGER>17</INTEGER>\n"
			"    </SimpleSequence>\n"
			"</NotSoSS>\n",
			nsss.to_xer().c_str());
}

TEST_F(CompoundBasic, ChoiceDecoderEncoder) {
	asn1::ExampleChoice ec;
	long val;

	val = 500;
	ec.set(ExampleChoice_PR_one_level);
	ec.One_level().set(val);
	ASSERT_STREQ(
			"<ExampleChoice>\n"
			"    <one-level>500</one-level>\n"
			"</ExampleChoice>\n",
			ec.to_xer().c_str());

	ASSERT_THROW(ec.Two_level(), std::runtime_error);

	val = 123;
	ec.set(ExampleChoice_PR_two_level);
	ec.Two_level().set(val);
	ASSERT_STREQ(
			"<ExampleChoice>\n"
			"    <two-level>123</two-level>\n"
			"</ExampleChoice>\n",
			ec.to_xer().c_str());

	ec.set(ExampleChoice_PR_some_string);
	asn1::IA5String str = ec.Some_string();
	str.set("HH");
	ASSERT_STREQ(
			"<ExampleChoice>\n"
			"    <some-string>HH</some-string>\n"
			"</ExampleChoice>\n",
			ec.to_xer().c_str());

	std::vector<uint8_t> ber = ec.to_ber();
	ASSERT_EQ(164, ber[0]);
	ASSERT_EQ(4,   ber[1]);
	ASSERT_EQ(22,  ber[2]);
	ASSERT_EQ(2,   ber[3]);
	ASSERT_EQ(72,  ber[4]);
	ASSERT_EQ(72,  ber[5]);
}

TEST_F(CompoundBasic, SequenceDecoderEncoder) {
	asn1::SomeSequence ss;
	ASSERT_THROW(ss.to_xer(), std::runtime_error);

	// CHOICE must be set to something, else we cannot encode with XER
	// TODO: better error reporting or what?
	asn1::ExampleChoice ec = ss.Ex_choice();
	ec.set(ExampleChoice_PR_two_altitude);
	ec.Two_altitude().set(30);

	ss.Ex_bool().set(true);
	std::string xer = ss.to_xer();
	ASSERT_STREQ(
			"<SomeSequence>\n"
			"    <ex-bool><true/></ex-bool>\n"
			"    <ex-int>0</ex-int>\n"
			"    <ex-choice>\n"
			"        <two-altitude>30</two-altitude>\n"
			"    </ex-choice>\n"
			"    <ex-null></ex-null>\n"
			"</SomeSequence>\n",
			xer.c_str());

	std::vector<uint8_t> ber = ss.to_ber();
	uint8_t exp_arr[] = { 48, 21, 160, 3 , 1 , 1 , 255, 161, 3 , 2 , 1 , 0 , 162, 5 , 163, 3 , 2 , 1 , 30, 168, 2 , 5 , 0};
	std::vector<uint8_t> exp(exp_arr, exp_arr + sizeof(exp_arr) / sizeof(exp_arr[0]));
	EXPECT_ARRAY_EQ(ber, exp);

	asn1::SomeSequence second;
	second.from_ber(&ber[0], ber.size());
	ASSERT_STREQ(second.to_xer().c_str(), xer.c_str());

	second.Ex_int().set(30);
	ASSERT_STRNE(second.to_xer().c_str(), xer.c_str());

	second.Ex_int().set(0);
	ASSERT_STREQ(second.to_xer().c_str(), xer.c_str());
}


TEST_F(CompoundBasic, Reserved) {
	asn1::Reserved reserved;
	reserved.Asm().set(30);

	ASSERT_STREQ(
			"<Reserved>\n"
			"    <int>0</int>\n"
			"    <asm>30</asm>\n"
			"    <delete></delete>\n"
			"</Reserved>\n",
			reserved.to_xer().c_str());
}

TEST_F(CompoundBasic, OptionalStructFields) {
    asn1::OneOptional oo;
    oo.fill();

    ASSERT_STREQ(oo.to_xer().c_str(),
	"<OneOptional>\n    "
	"<req-str></req-str>\n"
	"</OneOptional>\n");

    oo.Opt_str().set("haha");
    ASSERT_STREQ(oo.to_xer().c_str(),
		 "<OneOptional>\n"
		 "    <opt-str>haha</opt-str>\n"
		 "    <req-str></req-str>\n"
		 "</OneOptional>\n");
}

TEST_F(CompoundBasic, CopyTest_SeqEnum) {
    asn1::SeqEnum ss;
    asn1::SeqEnum ss2(ss);
    ss2.fill();
    asn1::SeqEnum ss3(ss2);
    ASSERT_STREQ(ss3.to_xer().c_str(),
		 "<SeqEnum>\n"
		 "    <ex-enum><bar/></ex-enum>\n"
		 "</SeqEnum>\n");
}

TEST_F(CompoundBasic, CopyTest_Seq2Enum) {
    asn1::Seq2Enum ss;
    asn1::Seq2Enum ss2(ss);
    ss2.fill();
    asn1::Seq2Enum ss3(ss2);
    ASSERT_STREQ(ss3.to_xer().c_str(),
		 "<Seq2Enum>\n"
		 "    <ex-enum2>\n"
		 "        <ex-enum><bar/></ex-enum>\n"
		 "    </ex-enum2>\n"
		 "</Seq2Enum>\n");
}
