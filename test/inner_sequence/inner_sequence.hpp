#include "_asn1/SeqInCho.h"
#include "_asn1/EnumInSeqInCho.h"

class Inner : public ::testing::Test {};

TEST_F(Inner, SequenceInChoice) {
	asn1::SeqInCho sic;
	sic.set(SeqInCho_PR_del);

	ASSERT_STREQ(
			"<SeqInCho>\n"
			"    <del></del>\n"
			"</SeqInCho>\n",
			sic.to_xer().c_str());

	sic.set(SeqInCho_PR_items);
	ASSERT_STREQ(
			"<SeqInCho>\n"
			"    <items>\n"
			"        <ex-bool><false/></ex-bool>\n"
			"        <ex-int>0</ex-int>\n"
			"    </items>\n"
			"</SeqInCho>\n",
			sic.to_xer().c_str());

    asn1::Long ex_int = sic.Items().Ex_int();
	ex_int.set(123);
	ASSERT_STREQ(
			"<SeqInCho>\n"
			"    <items>\n"
			"        <ex-bool><false/></ex-bool>\n"
			"        <ex-int>123</ex-int>\n"
			"    </items>\n"
			"</SeqInCho>\n",
			sic.to_xer().c_str());

	sic.Items().Ex_bool().set(true);
	ASSERT_STREQ(
			"<SeqInCho>\n"
			"    <items>\n"
			"        <ex-bool><true/></ex-bool>\n"
			"        <ex-int>123</ex-int>\n"
			"    </items>\n"
			"</SeqInCho>\n",
			sic.to_xer().c_str());

	asn1::SeqInCho sic2;
	sic2.from_xer(sic.to_xer());
	ASSERT_EQ(sic.is(SeqInCho_PR_items), sic2.is(SeqInCho_PR_items));
}

TEST_F(Inner, EnumInSeqInCho) {
  asn1::EnumInSeqInCho esc;
  asn1::EnumInSeqInCho esc2(esc);
  esc2.fill();

  // not yet implemented: recursive filler for inner sequences, so fails!!
  asn1::EnumInSeqInCho esc3(esc2);
}
