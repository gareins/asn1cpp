#include <gtest/gtest.h>

#define EXPECT_ARRAY_EQ(v1, v2) \
	EXPECT_EQ(v1.size(), v2.size()); \
	for(uint i = 0; i < v1.size(); i++) \
		EXPECT_EQ(v1[i], v2[i]);

// Tests
#include "basic/basic.hpp"
#include "noasn/noasm.hpp"
#include "compound_basic/compound_basic.hpp"
#include "inner_sequence/inner_sequence.hpp"


int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}



