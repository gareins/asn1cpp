#include "IA5String.h"
#include "BOOLEAN.h"
#include "../../src/asn1cpp.hpp"

class NoAsn : public ::testing::Test {};

TEST_F(NoAsn, IntegerDecoderEncoder) {
	asn1::Integer at;
	ASSERT_THROW(at.to_xer(), std::runtime_error);

	at.set(128);
	std::string xer = at.to_xer();
	ASSERT_STREQ("<INTEGER>128</INTEGER>\n", xer.c_str());

	std::vector<uint8_t> ber = at.to_ber();
	ASSERT_EQ(2,   ber[0]);
	ASSERT_EQ(2,   ber[1]);
	ASSERT_EQ(0,   ber[2]);
	ASSERT_EQ(128, ber[3]);

	asn1::Integer second;
	second.from_ber(&ber[0], ber.size());

	ASSERT_EQ(second.get(), 128);
	ASSERT_STREQ(second.to_xer().c_str(), xer.c_str());
}


TEST_F(NoAsn, StringDecoderEncoder) {
	asn1::IA5String str;
	ASSERT_THROW(str.to_xer(), std::runtime_error);
	str.fill();

	str.set("tst:)");
	std::string xer = str.to_xer();
	ASSERT_STREQ("<IA5String>tst:)</IA5String>\n", xer.c_str());

	std::vector<uint8_t> ber = str.to_ber();
	ASSERT_EQ(22,  ber[0]);
	ASSERT_EQ(5,   ber[1]);
	ASSERT_EQ(116, ber[2]);
	ASSERT_EQ(115, ber[3]);
	ASSERT_EQ(116, ber[4]);
	ASSERT_EQ(58,  ber[5]);
	ASSERT_EQ(41,  ber[6]);

	asn1::IA5String second;
	second.from_ber(ber);

	ASSERT_STREQ("tst:)", second.get().c_str());
	ASSERT_STREQ(second.to_xer().c_str(), xer.c_str());
}

TEST_F(NoAsn, BooleanDecoderEncoder) {
	asn1::Boolean b;
	ASSERT_THROW(b.to_xer(), std::runtime_error);
	b.fill();

	b.set(true);
	std::string xer = b.to_xer();
	ASSERT_STREQ("<BOOLEAN><true/></BOOLEAN>\n", xer.c_str());

	std::vector<uint8_t> ber = b.to_ber();
	ASSERT_EQ(1,   ber[0]);
	ASSERT_EQ(1,   ber[1]);
	ASSERT_EQ(255, ber[2]);

	asn1::Boolean second;
	second.from_ber(&ber[0], ber.size());

	ASSERT_EQ(second.get(), true);
	ASSERT_STREQ(second.to_xer().c_str(), xer.c_str());
}
