function file_exists(name)
   local f=io.open(name, "r")
   if f~=nil then io.close(f) return true else return false end
end

-- Test if libgtest exists
local gtest = "libgtest.a"
if not file_exists(gtest) then
  print(gtest .. " does not exits :(")
  os.exit(1)
end

workspace "Test"
  configurations { "default" }

project "Asn1cppTests"
  kind "ConsoleApp"
  language "C++"
  objdir "build/obj"
  targetdir "build"
  includedirs "asn1c_headers"
  warnings "Extra"
  symbols "On"

  buildoptions { "-pthread" }
  files { "**.h", "**.c", "**.cpp", "**.hpp" }
  linkoptions { gtest, "-lpthread" }
